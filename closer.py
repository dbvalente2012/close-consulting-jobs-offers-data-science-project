
import requests
from bs4 import BeautifulSoup 
import csv
import numpy as np
import pandas as pd
from collections import Counter 
import re 

import csv
diretoria_csv='C:\\Users\\User\\Desktop\\Closer.csv'
array_csv=[]

for con in range(1,5):
    url = (f'https://www.closer.pt/career/job-offers/?page={con}')
    

    pagina=requests.get(url)
    if pagina.status_code !=200:
        print(f'Erro no download da página: {pagina.status_code}')
        exit()

    soup=BeautifulSoup(pagina.text,'html.parser')

    paginas_total=soup.find('ul',class_='pagination').text.split()


    array_titulo=[]
    array_local=[]
    array_tipo_contrato=[]
    titulo=soup.find_all('h2',class_='heading')
    local=soup.find_all('div',class_='info')
    for  le_titulo in titulo:   
        titulo_final=le_titulo.text.strip()
        array_titulo.append(titulo_final)

    for  le_local in local:    
        local_final=le_local.text.strip()
        
        apenas_cidade=[]
        for conta_ate_local in range(0,30):
                if local_final[conta_ate_local]!=' ':
                    apenas_cidade.append(local_final[conta_ate_local])
                else:
                    break
        apenas_cidade="".join(apenas_cidade)
        apenas_cidade=apenas_cidade.strip(" -")
        apenas_cidade=apenas_cidade.rstrip() 
        array_local.append(apenas_cidade)

        if apenas_cidade in local_final:
            array_tcontrato=local_final.replace(apenas_cidade,'')
        
        array_tipo_contrato.append(array_tcontrato)

        


    array_local.remove("Job")
    array_tipo_contrato.remove(' Offers')
    tamanho_total=len(array_titulo)
    for conta_tudo in range (tamanho_total):
        cli_csv=[array_titulo[conta_tudo],array_local[conta_tudo],array_tipo_contrato[conta_tudo]]
        array_csv.append(cli_csv)

   
with open(diretoria_csv, 'w',newline='') as csvfile:          
                x = csv.writer(csvfile) 
                cabecalho=['Anuncio','Local','Tipo Contrato']
                x.writerow(cabecalho)
                for escreve_linha_csv in array_csv:
                    x.writerow(escreve_linha_csv)      
                #print('\n----------------------------------------------------------\nAcabou de guardar esta informação no ficheiro ',diretoria_csv,'\n')   

contas=pd.read_csv(diretoria_csv)
df_contas=pd.DataFrame(contas)
print(df_contas)

print('\n')
num_total_ofertas=len(df_contas)
print('Total de ofertas',num_total_ofertas)
print('\n')
dups = df_contas.pivot_table(index = ['Local'], aggfunc ='size') 
print(dups.to_string(index=True))
print('\n')

def procura_candidato(df_contas):
    Tipo=[]
    texto=df_contas['Anuncio']
    if 'Analyst' in texto:
        Tipo.append('Analyst')
    if 'Developer' in  texto:
        Tipo.append('Developer')
    if 'Engineer' in  texto:
        Tipo.append('Engineer')
    if 'Consultant' in  texto:
        Tipo.append('Consultant')
    if 'Scientist' in  texto:
        Tipo.append('Scientist')
    if 'Administrator' in  texto:
        Tipo.append('Administrator')
    if 'Manager' in  texto:
        Tipo.append('Manager')
    if 'Programmer' in  texto:
        Tipo.append('Programmer')
    if 'Support' in  texto:
        Tipo.append('Support')
    if 'Specialist' in  texto:
        Tipo.append('Specialist')
    if 'Actuary' in  texto:
        Tipo.append('Actuary')
    if 'Tester' in  texto:
        Tipo.append('Tester')
    if 'Architect' in  texto:
        Tipo.append('Architect')
    Tipo = ' '.join([str(elemento) for elemento in Tipo]) 
    return (Tipo)
    
df_contas['Tipo']=df_contas.apply(procura_candidato,axis=1)
print(df_contas)

print('\n')
dups = df_contas.pivot_table(index = ['Tipo'], aggfunc ='size') 
print(dups.to_string(index=True))
print('\n')

def procura_candidato_tipo_trabalho(df_contas):
    Tipo_trabalho=[]
    texto=df_contas['Anuncio']
    if 'Junior'in texto and 'Mid' in texto:
        Tipo_trabalho.append('Junior/Mid')
    elif 'Junior'in texto:
        Tipo_trabalho.append('Junior')
    elif 'Senior' in  texto:
        Tipo_trabalho.append('Senior')
    elif 'Mid' in  texto:
        Tipo_trabalho.append('Mid')
    else:
        Tipo_trabalho.append('Sem Info')
    
    Tipo_trabalho = ' '.join([str(elemento) for elemento in Tipo_trabalho]) 
    return (''.join(Tipo_trabalho))
    
df_contas['Anos experiencia']=df_contas.apply(procura_candidato_tipo_trabalho,axis=1)
print(df_contas)


print('\n')
dups = df_contas.pivot_table(index = ['Anos experiencia'], aggfunc ='size') 
print(dups.to_string(index=True))
print('\n')

print('\n')
Porto_tabela=df_contas.loc[df_contas['Local']=='Porto']
print(Porto_tabela)
num_total_Porto_tabela=len(Porto_tabela)
print('Total de ofertas do Porto',num_total_Porto_tabela)
print('\n')
Lisbon_tabela=df_contas.loc[df_contas['Local']=='Lisbon']
print(Lisbon_tabela)
num_total_Lisbon_tabela=len(Lisbon_tabela)
print('Total de ofertas de Lisboa',num_total_Lisbon_tabela)
print('\n')
Remoto_tabela=df_contas.loc[df_contas['Local']=='Remote']
print(Remoto_tabela)
num_total_Remoto_tabela=len(Remoto_tabela)
print('Total de ofertas Remoto',num_total_Remoto_tabela)